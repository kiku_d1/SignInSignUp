//
//  ViewController.swift
//  SignInSignUp
//
//  Created by Адина on 12/14/23.
//
import UIKit
import SnapKit

class ViewController: UIViewController {

    private lazy var loginView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 30
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return view
    }()
    private lazy var loginLabel: UILabel = {
        let view = UILabel()
        view.text = "Employee Id / Email"
        view.textColor = .init(hex: "#A5A5A5")
        view.font = .systemFont(ofSize: 12)
        return view
    }()
    private lazy var loginTF: UITextField = {
        let view = UITextField()
        view.placeholder = "Email"
        return view
    }()
    private lazy var passwordLabel: UILabel = {
        let view = UILabel()
        view.text = "password"
        return view
    }()
    private lazy var passwordTF: UITextField = {
        let view = UITextField()
        return view
    }()
//    private lazy var 
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .init(hex: "#047494")
        setupUI()
    }

    @objc func setupUI(){
        view.addSubview(loginView)
        loginView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.horizontalEdges.equalToSuperview()
            make.height.equalTo(view.frame.height / 2)
        }
        loginView.addSubview(loginLabel)
        loginLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(43)
            make.left.equalToSuperview().offset(23)
        }
        loginView.addSubview(loginTF)
        loginTF.snp.makeConstraints { make in
            make.top.equalTo(loginLabel.snp.bottom).offset(5)
            make.horizontalEdges.equalToSuperview().inset(23)
        }
    }

}

